package ana;

import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Map.Entry;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class textana extends JFrame{
	
	double NumLines=0, NumBL=0,NumSpace = 0,NumWords=0,NumChars=0;
	double ACPL,AWL;
	double NumLines2=0, NumBL2=0,NumSpace2 = 0,NumWords2=0,NumChars2=0;
	double ACPL2,AWL2;	
	double increaseOfLines,increaseOfBL,increaseOfNBL,increaseOfWords,increaseOfCC,increaseOfSpace,increaseOfChars;
	double NBLines = 0,NBLines2=0;
	double characterCount = 0;	 
	String mostRepeatedWord1,mostRepeatedWord2=null;
	JFrame frame = new JFrame("Text Files Analyzer");
	JPanel panel1 = new JPanel();
	JPanel panel2 = new JPanel();
	JPanel panel3 = new JPanel();
	JLabel lb1	= new JLabel("input file name:");	
	JButton bt1 =new JButton("analyze");
	JButton bt2 =new JButton("Remove punctuation and possession");
	JButton bt3 =new JButton("help info");	
	JTextField resultfield= new JTextField();
	JTextField resultfield1= new JTextField();
	JTextField resultfield2= new JTextField();
	JTextField resultfield3= new JTextField();
	JTextField resultfield4= new JTextField();
	JTextField resultfield5= new JTextField();
	JTextField resultfield6= new JTextField();
	JTextField resultfield7= new JTextField();
	
	JTextField fileNameField= new JTextField(9);
	
	public textana(){
		frame.setSize(500,600);
		
		panel1.setLayout(new GridLayout(5,1,0,50));
		panel1.add(lb1);
		panel1.add(fileNameField);
		panel1.add(bt1);
		panel1.add(bt2);
		panel1.add(bt3);	

		
		panel3.setLayout(new GridLayout(7,1,0,50));
		panel3.add(resultfield1);
		panel3.add(resultfield2);
		panel3.add(resultfield3);
		panel3.add(resultfield4);
		panel3.add(resultfield5);
		panel3.add(resultfield6);
		panel3.add(resultfield7);
		
		panel2.setLayout(new GridLayout(1,2));
		panel2.add(panel1);
		panel2.add(panel3);	
		frame.add(panel2);
		frame.setVisible(true);
		bt1.addActionListener(new action1());
		bt2.addActionListener(new action2());
		bt3.addActionListener(new action3());
	}
	
	private LayoutManager GridLayout(int i, int j, int k, int l) {
		// TODO Auto-generated method stub
		return null;
	}

	class action1 implements ActionListener {
		public void actionPerformed(ActionEvent e1){ 
			increaseOfLines=0;increaseOfBL=0;increaseOfNBL=0;increaseOfWords=0;increaseOfCC=0;increaseOfSpace=0;increaseOfChars=0;
			
			String fileName=fileNameField.getText();
//find the number of lines
			FileInputStream fstream = null;
			try {
				fstream = new FileInputStream(fileName);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			String strLine;
			

			try {
				while ((strLine = br.readLine()) != null){		
					increaseOfLines++;
					NumLines++;
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    		
// find the number of blank lines
			BufferedReader reader = null;
			try {
				reader = new BufferedReader(new FileReader(fileName));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			String line;
			try {
				while ((line = reader.readLine()) != null){
				    if(!"".equals(line.trim())){
				        NBLines++;
				        increaseOfNBL++;
				    }
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    		NumBL=NumLines-NBLines;	    		
	    		increaseOfBL=increaseOfLines-increaseOfNBL;

	    		
	    
//number of words
	    		File file = new File(fileName);
	    		try(Scanner sc = new Scanner(new FileInputStream(file))){    		    
	    		    while(sc.hasNext()){
	    		        sc.next();
	    		        NumWords++;
	    		        increaseOfWords++;
	    		    }    		
	    		} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    //find the number of spaces
	    		NumSpace=NumWords-NumLines+NumBL;		
	    
	    //find the number of chars
	    		File file1 = new File(fileName);
	            FileInputStream fileStream = null;
				try {
					fileStream = new FileInputStream(file1);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	            InputStreamReader input = new InputStreamReader(fileStream);
	            BufferedReader reader1 = new BufferedReader(input);
	             
	            String line1;          
	                     	            	            
	            try {
					while((line1 = reader1.readLine()) != null)
					{              
					    if(!(line1.equals("")))
					    {			         
					    	characterCount += line1.length();	
					    	increaseOfCC+=line1.length();

					    }
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}	            	           
	            NumChars=characterCount-NumSpace;
	      
	     // average chars per line
	            ACPL=NumChars/NumLines;
	     //average word length
	            AWL=NumChars/NumWords;
	     //most common word
	            mostRepeatedWord2=mostRepeatedWord1;
	    		HashMap<String, Integer> wordCountMap = new HashMap<String, Integer>();    	
	    		BufferedReader reader11 = null;   		
	    		try
	    		{	
	    			reader11 = new BufferedReader(new FileReader(fileName));		
	    			String currentLine = reader11.readLine();	    			
	    			while (currentLine != null)
	    			{					
	    				String[] words = currentLine.toLowerCase().split(" ");
	    				for (String word : words)
	    				{
	    					if(wordCountMap.containsKey(word))
	    					{	
	    						wordCountMap.put(word, wordCountMap.get(word)+1);
	    					}
	    					else
	    					{
	    						wordCountMap.put(word, 1);
	    					}
	    				}			
	    				currentLine = reader11.readLine();
	    			}    			
	    			int count = 0;    			
	    			Set<Entry<String, Integer>> entrySet = wordCountMap.entrySet();   			
	    			for (Entry<String, Integer> entry : entrySet)
	    			{
	    				if(entry.getValue() > count)
	    				{
	    					mostRepeatedWord1 = entry.getKey();	    					
	    					count = entry.getValue();
	    				}
	    			}	    			    			
	    		} 
	    		catch (IOException e) 
	    		{
	    			e.printStackTrace();
	    		}
	    		finally
	    		{
	    			try 
	    			{
	    				reader11.close();           //Closing the reader
	    			}
	    			catch (IOException e) 
	    			{
	    				e.printStackTrace();
	    			}
	    		}

	            
	    		resultfield1.setText("number of lines:"+NumLines);
	    		resultfield2.setText("number of blank lines:"+NumBL);
	    		resultfield3.setText("number of spaces:"+NumSpace);
	    		resultfield4.setText("number of words:"+NumWords);
	    		resultfield5.setText("average chars per line:"+ACPL);
	    		resultfield6.setText("average word length:"+AWL);
	    		resultfield7.setText("most common words: "+mostRepeatedWord1);	


		}
	}
		class action2 implements ActionListener{
			public void actionPerformed(ActionEvent e2){
				NumLines2=NumLines-increaseOfLines;
	    		NumBL2=NumBL-increaseOfBL;
	    		NumWords2=NumWords-increaseOfWords;
	    		NumSpace2=NumWords2-NumLines2+NumBL2;
	    		increaseOfSpace=NumSpace-NumSpace2;
	    		NBLines2=NBLines-increaseOfNBL;	    		
	    		increaseOfChars=increaseOfCC-increaseOfSpace;    
	    		NumChars2=NumChars-increaseOfChars;	    
	    		AWL2=NumChars2/NumWords2;
	    		ACPL2=NumChars2/NumLines2;
	    		
	    		
				NumLines=NumLines2;
				NumBL=NumBL2;
				NumSpace = NumSpace2;
				NumWords=NumWords2;
				NumChars=NumChars2;
				ACPL=ACPL2;
				AWL=AWL2;
	    		NBLines=NBLines2;
	    		characterCount=characterCount-increaseOfCC;
	    		
				resultfield1.setText("number of lines:"+NumLines2);
	    		resultfield2.setText("number of blank lines:"+NumBL2);
	    		resultfield3.setText("number of spaces:"+NumSpace2);
	    		resultfield4.setText("number of words:"+NumWords2);
	    		resultfield5.setText("average chars per line:"+ACPL2);
	    		resultfield6.setText("average word length:"+AWL2);
	    		resultfield7.setText("most common words "+mostRepeatedWord2);		  
			}
		}
		class action3 implements ActionListener{
			public void actionPerformed(ActionEvent e3) {
			JFrame frameee=new JFrame ("help information");
			frameee.setVisible(true);
			frameee.setSize(500,500);
			JLabel lbh1= new JLabel("This test analyzer can help people to analyze a txt file");
			JLabel labh2= new JLabel("you need to enter the filename and click analyze button it will return the result");
			JLabel labh3 = new JLabel("delete button: delete the result and return the previous result");
			JPanel panelh= new JPanel();
			frameee.add(panelh);
			panelh.add(lbh1);
			panelh.add(labh2);
			panelh.add(labh3);
				
			}
		}
		
}